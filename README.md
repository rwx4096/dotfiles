# My dotfiles

I aim to keep my system very minimal, as reflected by my dotfiles. They configure only the following:
- [Doas](https://github.com/Duncaen/OpenDoas) ([etc/doas.conf](https://gitlab.com/rwx4096/dotfiles/-/blob/main/etc/doas.conf)): Sudo alternative, but superior in every way.
- [Qutebrowser](https://qutebrowser.org/) ([.config/qutebrowser](https://gitlab.com/rwx4096/dotfiles/-/tree/main/.config/qutebrowser)): Lightweight web browser, with vim-like keybinds.
- [Helix](https://helix-editor.com/) ([.config/helix](https://gitlab.com/rwx4096/dotfiles/-/tree/main/.config/helix)): Post-modern, vim-like text editor.
